package edu.ceis.ssicerviseceis;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SsiCerviseCeisApplication {
//CDI contexto de inyeccion
  public static void main(String[] args) {
    SpringApplication.run(SsiCerviseCeisApplication.class, args);
  }
}
