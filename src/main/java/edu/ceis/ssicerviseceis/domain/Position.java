package edu.ceis.ssicerviseceis.domain;

import javax.persistence.Column;
import javax.persistence.Entity;

@Entity
public class Position extends ModelBase {

  @Column
  private String name;

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }
}
